package scratches.gradlebootdocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleBootDockerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GradleBootDockerApplication.class, args);
    }

}
